import twitter
import csv
from random import randint
from time import sleep
import os.path
import sys
import json
from datetime import datetime

def log(text):
    current_datetime = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    with open(log_filename, 'a', encoding='utf-8') as log_file:
        log_file.write(current_datetime + ' > ' + text + '\n')
    print(text)

# Debug: true not to send Tweets.
debug = True

# Twitter API tokens.
consumer_key        = ''
consumer_secret     = ''

access_token        = ''
access_token_secret = ''

# Name of the bot settings JSON file.
settings_json_filename = 'settings.json'

# The amount of time separating each Tweet.
time_period = 60.0 * 30

# The name of the words database file.
database_filename = 'Lexique383.tsv'

# The name of the file to store sent words.
sent_words_filename = 'sent_words.txt'

# The name of the file to write the log.
log_filename = 'log.txt'

log("Now starting...")

# Check if the file exists.
if not os.path.isfile(settings_json_filename):
    sys.exit('The file ' + settings_json_filename + ' does not exist.')

# Check if the file exists.
if not os.path.isfile(database_filename):
    sys.exit('The file ' + database_filename + ' does not exist.')

# Check if the file exists.
if not os.path.isfile(sent_words_filename):
    sys.exit('The file ' + sent_words_filename + ' does not exist.')

# Creates the variable which will contain the API handle.
api = None

# Opens and reads the JSON settings file.
log('Reading the settings file (' + settings_json_filename + ')...')
with open(settings_json_filename, encoding='utf-8') as settings_json_file:
    settings_json = json.loads(settings_json_file.read())
    debug               = settings_json['debug']
    consumer_key        = settings_json['consumer_key']
    consumer_secret     = settings_json['consumer_secret']
    access_token        = settings_json['access_token']
    access_token_secret = settings_json['access_token_secret']

log('Settings successfully loaded.')

# Gets the words not to add.
log('Reading the sent words file (' + sent_words_filename + ')...')
words_not_to_add = []
with open(sent_words_filename, encoding='utf-8') as sent_words_file:
    for row in sent_words_file:
        words_not_to_add.append(row.rstrip())

log('Initializing dictionnary...')
rows = []

# Opens the file.
with open(database_filename, encoding='utf-8') as lexique:
    # Populates the dict.
    reader = csv.DictReader(lexique, delimiter = '\t')

    # Goes through all the lines.
    for row in reader:
        # Not adding words already used.
        if row['ortho'] in words_not_to_add:
            continue
        
        # Gets only names.
        if row['cgram'] == 'NOM':
            rows.append(row)

# Removes the header row.
rows.pop(0)

# Main loop.
while True:
    # Chooses a random row.
    random_index = randint(0, len(rows) - 1)

    # Gets the row at the specified index.
    random_row = rows[random_index]

    # Gets the word from the row.
    random_word = random_row['ortho']

    log('Picked word "' + random_word + '".')

    # Adds the feminine mark if needed.
    feminine = ''
    if random_row['genre'] == 'f':
        feminine = 'e'

    # Adds the plural mark if needed.
    plural = ''
    if random_row['nombre'] == 'p':
        plural = 's'

    # Adds an upper case letter.
    random_word_capitalized = random_word[0].upper() + random_word[1:]

    # Creates the sentence to send.
    sentence = random_word_capitalized + ' confiné' + feminine + plural + '.'
    log('Tweet content: "' + sentence + '"')

    # Sends the sentence to Twitter.
    if not debug:
        log('Connecting to Twitter API...')
        # Connects to Twitter.
        api = twitter.Api(
            consumer_key,
            consumer_secret,
            access_token,
            access_token_secret
        )
        log("Sending Tweet...")
        api.PostUpdate(sentence)
        log('Tweet sent.')
        del api
        api = None

    # Writes the word into a file to avoid reusing it.
    with open(sent_words_filename, 'a', encoding='utf-8') as sent_words_file:
        sent_words_file.write(random_word + '\n')
    
    # Removes the used row from the array.
    rows.pop(random_index)

    # Waits for the given period of time.
    sleep(time_period)

# Main loop end.