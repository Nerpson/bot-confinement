# Bot du confinement

Ce simple script Python a pour vocation de -d'une part- se refamiliariser rapidement avec le langage, et de s'amuser à créer un bot Twitter autour du confinement en France.

## Que fait ce bot ?

Le bot génère de courtes phrases nominales composées d'un nom commun suivi de l'adjectif "confiné" accordé en genre et en nombre. Le nom commun est choisi aléatoirement dans un dictionnaire de 140 000 mots de [Boris New & Christophe Pallier](http://www.lexique.org) (Lexique383.tsv).

## Peut-on voir le résultat ?

Le bot est actif et ses tweets peuvent être consultés à cette adresse :  https://twitter.com/BotConfinement

## Installation

1. Cloner le repository
```bash
git clone git@gitlab.com:Nerpson/bot-confinement.git
```
2. Installer [Python 3](https://www.python.org/downloads/)
3. Installer [`python-twitter`](https://github.com/bear/python-twitter) avec la commande suivante
```bash
pip install --user python-twitter
```
4. Configurer le bot avec un fichier `settings.json` respectant la structure suivante
```json
{
	"debug": false,
	"consumer_key": "Votre consumer key ici",
	"consumer_secret": "Votre consumer secret ici",

	"access_token": "Votre jeton accès ici",
	"access_token_secret": "Votre jeton secret d'accès ici"
}
```
5. Désactiver le mode debug en mettant `false` dans le fichier JSON, pour pouvoir se connecter à l'API de Twitter et envoyer les tweets.